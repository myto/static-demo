//设置产出的文件编码为utf8
fis.config.set('project.charset', 'utf8');

fis.config.set('modules.postpackager', 'simple');
//开始autoCombine可以将零散资源进行自动打包
fis.config.set('settings.postpackager.simple.autoCombine', true);
fis.config.set("server", {
    server: ".",
    dest: "./output",
    dot: {
        "src": "app/demo",
        "dest": "app/modules/tpl"
    }
});
//设置当前的版本号
var version = "1.0.0";
//http://127.0.0.1:1001/dujia/test/
var jsDomain = "",
    cssDomain = "",
    imgDomain = "";
//配置自动编译的插件
fis.config.merge({
    roadmap: {
        ext: {
            md: 'html',
            sass: 'css',
            less: 'css',
            dot: 'js'
        }
    },
    modules: {
        parser: {
            md: 'marked',
            sass: 'sass',
            less: 'less',
            dot: 'dot'
        },
        postprocessor: {
            js: 'jswrapper, require-async'
        },
        // prepackager: ['dot'],
        postpackager: ['autoload', 'simple']
    },
    settings: {
        postprocessor: {
            jswrapper: {
                //             //wrap type. if omitted, it will wrap js file with '(function(){...})();'.
                type: 'amd'
                //             //you can use template also, ${content} means the file content
                //            template : '!function(){${content}}();'
                //             //wrap all js file, default is false, wrap modular js file only.
                //            wrapAll : true
            }
        },
        postpackager: {
            modjs: {
                subpath: 'pkg/map.js'
            }
        }
    },
    deploy: {
        local: {
            //from参数省略，表示从发布后的根目录开始上传
            //发布到当前项目的上一级的output目录中
            to: './output',
            exclude: /\/node\_modules\//i
        }
    }
});
//useSiteMap设置使用整站/页面异步资源表配置，默认为false
fis.config.set('settings.postpackager.autoload.useSiteMap', true);


fis.config.set('pack', {
    '/js/mod.js': [
        '/app/assets/lib/mod.js',
        '/app/modules/common/**.js',
        '/app/modules/tc/common/**.js'
        // ,
        // '/app/modules/tpl/city/index.js'
    ]
});

//源码编译时屏蔽 node_modules,bin文件夹，屏蔽tch.js,package.json
fis.config.set('project.exclude', /^(\/node\_modules)|^(\/bin)|(tch\S*\.js)|(package\.json)/i);
fis.config.set('modules.lint.js', 'jshint');
fis.config.set('modules.test.js', 'phantomjs');
fis.config.set('modules.optimizer.js', 'uglify-js');

fis.config.set('modules.spriter', 'csssprites');

var now = new Date();
fis.config.set('timestamp', [now.getFullYear(), now.getMonth() + 1, now.getDate(), now.getHours()].join(''));
//为所有样式资源开启csssprites

fis.config.set('roadmap.path', [
    {
        reg: /\/app\/modules\/async\/(.*)\/index\.js/,
        release: version+"/js/sync/$1.js",
        isMod: true,
        id: 'async/$1/index',
        usePostprocessor : true,
        type : 'amd',
        useDomain: true,
        query: '?t=${timestamp}',
        domain: jsDomain
    },
//    {
//        reg: /\/app\/assets\/images\/activity\/(.*\.jpg)/,
//        release: version+'/img/$1',
//        useDomain: true,
//        domain: imgDomain,
//        query: '?t=${timestamp}'
//    },
    {
        reg: /\/app\/modules\/async\/(.*)\/index\.(.*ss)/,
        release: version+'/css/sync/$1.$2',
        useDomain: true,
        useSprite: true,
        query: '?t=${timestamp}',
        domain: cssDomain
    },
    {
        reg: "/js/mod.js",
        release: version+"/js/mod.js",
        useDomain: true,
        query: '?t=${timestamp}',
        domain: jsDomain
    },
//    {
//        reg: "/js/search.js",
//        release: version+"/js/search.js",
//        useDomain: true,
//        query: '?t=${timestamp}',
//        domain: jsDomain
//    },
//    {
//        reg: "/js/search.js",
//        release: version+"/js/search.js",
//        useDomain: true,
//        query: '?t=${timestamp}',
//        domain: jsDomain
//    },

    {
        reg: /^\/app\/assets\/images\/(.*\.gif)/,
        release: version+'/img/$1',
        useDomain: true,
        domain: imgDomain
    },
    {
        reg: /^\/app\/modules\/tc\/([^\/]*)\/(.*\.png)/,
        release: version+'/img/$1_$2',
        useDomain: true,
        domain: imgDomain
    },
    {
        reg: /^\/app\/modules\/async\/([^\/]*)\/(.*\.png)/,
        release: version+'/img/$1_$2',
        useDomain: true,
        domain: imgDomain
    },
    {
        reg: /^\/pkg\/(.*\.js)/,
        release: version+'/js/$1',
        useDomain: true,
        query: '?t=${timestamp}',
        domain: jsDomain
    },
    {
        reg: /^\/pkg\/(.*\.css)/,
        release: version+'/css/$1',
        useDomain: true,
        query: '?t=${timestamp}',
        domain: cssDomain
    },
    {
        reg: '**.*ss',
        useSprite: true
    },
    {
        //modules目录下的其他文件
        reg : /\/modules\/(.*)\.(js|coffee)$/i,
        //是组件化的，会被jswrapper包装
        isMod : true,
        //id是去掉modules和.js后缀中间的部分
        id : '$1',
        usePostprocessor : true,
        type : 'amd'
    },
    {
        //前端模板
        reg : '**.dot',
        //当做类js文件处理，可以识别__inline, __uri等资源定位标识
        isJsLike : true,
        release: false
    }
]);
fis.config.set('settings.spriter.csssprites.margin', 20);
fis.config.set('settings.spriter.csssprites', {
    scale: 0.5
});